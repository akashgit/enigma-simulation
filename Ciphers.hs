        
{- 
    COM2001 - Advanced Programming Topics
    Assignment 1 - Substitution Cipher

    Vaclav Hudec

    date: 19/10/2010
-}

module Ciphers where
    
    import Char
    
    -- data structure for substitution cipher ----------------------------------
    type Cipher = [(Char, Char)]
    
    cipher :: Cipher
    cipher = [  ('A', 'H'),
                ('B', 'T'),
                ('C', 'E'),
                ('D', 'S'),
                ('E', 'W'),
                ('F', 'Q'),
                ('G', 'K'),
                ('H', 'L'),
                ('I', 'J'),
                ('J', 'U'),
                ('K', 'I'),
                ('L', 'D'),
                ('M', 'R'),
                ('N', 'M'),
                ('O', 'F'),
                ('P', 'Z'),
                ('Q', 'A'),
                ('R', 'C'),
                ('S', 'P'),
                ('T', 'O'),
                ('U', 'Y'),
                ('V', 'B'),
                ('W', 'N'),
                ('X', 'V'),
                ('Y', 'G'),
                ('Z', 'X')]
      
    -- validating function -----------------------------------------------------
                
    -- validate that there are no duplicated letters in the cipher
    validateCipher :: Cipher -> Bool
    validateCipher [] = error "The cipher is empty"
    validateCipher (head:tail) = validateCipherA tail [head] 
    
    validateCipherA :: Cipher->Cipher -> Bool
    validateCipherA [] _ = True
    -- checked list contains pairs of letters in the cipher which have already been checked
    validateCipherA (firstCipher:restCipher) checked
        -- if, after filtering out one of the letters in each checked pair, a letter remains, it means it has been duplicated
        | not (null (filter (\char -> char == fst firstCipher) (map fst checked)))
            ||
          not (null (filter (\char -> char == snd firstCipher) (map snd checked))) = False -- therefore we return false
        | otherwise = validateCipherA  restCipher (firstCipher:checked) -- if no letter is duplicated so far,
                                                                        -- continue checking the rest of the cipher
                                                                        
    -- encoding functions ------------------------------------------------------
    
    encode :: Char->Cipher->Int -> Char    
    encode char cipher offset
        | null foundCipher = '.' -- unmatched character is returned as full stop
        | otherwise = snd (head foundCipher) -- a substitution cipher has been found, return it
            where
                plain = map fst cipher -- extract the plain from the cipher
                substitution = shiftChars (map snd cipher) offset -- apply offset to substitution chars
                offsetCipher = (zip plain substitution) -- cipher on which an offset has been applied
                foundCipher = (filter (\c -> char == fst c) offsetCipher) -- search for given character
    
    -- if offset is to be applied
    shiftChars :: String->Int -> String
    shiftChars chars 0 = chars -- no more shifting needed
    shiftChars chars offset = shiftChars ((last chars):(init chars)) (offset - 1)   -- shift letters to the right:
                                                                                    -- recursively take last letter
                                                                                    -- and append to the front
    encodeMessage :: String->Cipher->Int -> String
    encodeMessage [] _ _ = [] -- stopping condition: no more string is passed to be encoded
    -- encode the head and append to tail which then pass to be encoded as well
    encodeMessage (first:rest) cipher offset = (encode first cipher offset):(encodeMessage rest cipher offset)
    
    -- decoding functions ------------------------------------------------------
    
    reverseEncode :: Char->Cipher->Int -> Char
    reverseEncode char cipher offset =  let revCipher = map (\pair -> (snd pair, fst pair)) cipher  -- swap the substitution    
                                            len = length cipher                                     -- cipher with the plain
                                        in
                                            encode char revCipher (len - (mod offset len))  -- use encode with swapped cipher
                                                                                            -- and reversed offset
    -- same as "encodeMessage" but reverseEncode function is used
    reverseEncodeMessage :: String->Cipher->Int -> String
    reverseEncodeMessage [] _ _ = []
    reverseEncodeMessage (first:rest) cipher offset = (reverseEncode first cipher offset):(reverseEncodeMessage rest cipher offset)
    
    -- statistical function ----------------------------------------------------
    
    letterStats :: String -> [(Char, Int)]
    letterStats [] = [] -- stopping condition
    letterStats (head:tail) =   let frequency = letterFrequency tail [(head, 1)] -- number of occurences of each letter in string
                                    percentage = map snd frequency
                                    letters = map fst frequency
                                in
                                    zip letters (map round (rescale percentage)) -- pair the percentage with corresponding letter
                                    
    -- count the number of occurences of each letter
    letterFrequency :: String->[(Char, Float)] -> [(Char, Float)]
    letterFrequency [] checked = checked
    letterFrequency (head:tail) checked -- keep track of already found letters
        | not (null (filter (\char -> fst char == head) checked)) = -- current letter (head) has already been found
                -- increase the occurence of the letter by one
                letterFrequency tail (map (\pair -> if head == fst pair then (head, (snd pair) + 1) else pair) checked)
        | otherwise = letterFrequency tail ((head, 1):checked)  -- this letter is being looked at for the first
                                                                -- time; add it to the list of checked letters
    -- take the values and rescale to 0 - 100 percent
    rescale :: [Float] -> [Float]
    rescale values = let max = foldr (\current maximum -> if current < maximum then maximum else current) 0 values
                         sum = foldr (+) 0 values
                     in
                         map (\val -> ((1 / sum) * val) * 100) values
                         
    -- partial decode ----------------------------------------------------------

    partialDecode :: String->Cipher -> String
    partialDecode [] _ = [] -- stopping condition: no more string is passed
    -- decode with reverseEncode; if '.' is returned, substitution cipher was not found -> leave the original char
    --                            else convert the char to lower case and append to the head of the message
    -- then continue applying the same principle on the tail of the message
    partialDecode (head:tail) cipher = (if decodedChar /= '.' then toLower decodedChar else head):(partialDecode tail cipher)
        where decodedChar = reverseEncode head cipher 0 -- decode message with given cipher
    
    -- substitution cipher for mystery message
    mc :: Cipher
    mc = [
            ('E','W'),
            ('T','J'),
            ('H','C'),
            ('Z','U'),
            ('I','Q'),
            ('P','V'),
            ('O','F'),
            ('C','L'),
            ('R','E'),
            ('G','M'),
            ('F','T'),
            ('M','P'),
            ('A','X'),
            ('N','Y'),
            ('L','N'),
            ('B','B'),
            ('K','Z'),
            ('Y','R'),
            ('U','D'),
            ('D','H'),
            ('V','K'),
            ('W','S'),
            ('S','A')   
            ]
            
    {- mystery message translated as:
    
        itseasytobreakasubstitutioncipherprovidedyouhavealongenoughmessagestop
        letsmakethisonealittlebitlongerstop
        okitshouldbetherightsortofsizenowstop
        maybenotletsincreasethemessagelengthabitmorestop
    -}

    















