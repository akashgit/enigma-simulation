
module Assignment3 where 
import Assignment2
import Data.List
type Crib = [LetterPair]
type Connection = (Int,Char,Char)
type SteckerPair = LetterPair
type Start = Int

result :: (Maybe a) -> a
result (Just x) = x

breakEnigma :: Crib->Start-> Maybe (Offsets, Stecker)
breakEnigma crib start = breakEA crib [(fst (crib!!start),'A')] (0,0,start)

breakEA :: Crib->Stecker->Offsets -> Maybe (Offsets, Stecker)
breakEA crib stecker offsets =
	let
		fs = findStecker crib stecker offsets
	in
		if ((25,25,25) == offsets &&  fs == Nothing) then Nothing
		else if  (fs == Nothing) then breakEA crib stecker (offset_step(offsets)) 
				else Just (offsets,result fs)

findStecker :: Crib->Stecker->Offsets->Maybe Stecker
findStecker crib ((a,b):t) offsets
	|chaseImplications ((a,b):t) crib offsets /= Nothing = chaseImplications ((a,b):t) crib offsets 
	|nextLetter b /= Nothing = findStecker crib ((a,result(nextLetter b)):t) offsets
	|otherwise = Nothing 

nextLetter:: Char -> Maybe Char
nextLetter ch 
	|findIndex(\x -> x==ch) ['A'..'Z'] == Just 25 = Nothing
	|otherwise = Just (['A'..'Z']!!(1+result((findIndex(\x -> x==ch) ['A'..'Z']))))


chaseImplications :: Stecker->Crib-> Offsets->Maybe Stecker
chaseImplications [] _ _ = Nothing
chaseImplications (h:t) crib offsets = followConnections (findConnections h crib) crib (h:t) offsets

findConnections :: (Char,Char)->Crib->[Connection]
findConnections (ch1,ch2) crib = (map(\n ->(n,ch2, snd(crib!!n)) ) (findIndices (\x-> fst x ==ch1) crib))++(map(\n ->(n,ch1, snd(crib!!n)) ) (findIndices (\x-> fst x ==ch2) crib))

followConnections :: [Connection]->Crib->Stecker->Offsets->Maybe Stecker
followConnections [] _ s _ = Just s
followConnections (h:t) crib stecker offsets
		|(followConnection h crib stecker offsets) == Nothing = Nothing
		|otherwise = followConnections t crib (result(followConnection h crib stecker offsets)) offsets

followConnection :: Connection->Crib->Stecker->Offsets-> Maybe Stecker
followConnection (n, ini, c) crib s offset = 
	let
		sa = steckerAdd (enigmaEncode ini defaultEnigma (offsetN offset (n+1)), c) s
	in
		if (sa == Nothing) then Nothing
		else if (sa == Just s) then Just s
			else chaseImplications ( result sa) crib offset

steckerAdd :: SteckerPair->Stecker-> Maybe Stecker
steckerAdd sp s 
	|[] /= (filter(\n -> (n==sp||n==(snd sp,fst sp))) s) = Just s
	|null (filter(\n -> ((fst n == fst sp)
										||(fst n == snd sp)
										||(snd n ==fst sp)
										||(snd n == snd sp)) ) s ) = Just (sp:s)
	|otherwise = Nothing
