
module Assignment2 where 
import AssignmentHelp
type Offsets = (Int,Int,Int)
type Rotor = String -- type for a rotor
type LetterPair = (Char,Char)
type Reflector = [LetterPair] -- Stores leter pairs for reflector

type Stecker = [LetterPair] --Stores leter pairs for Steckerboard
{- date type for simple and stekered enigmas. they store all data needed for enigma to run
(Rotors and their offsets, reflectors and Steckerboard for SteckeredEnigma)-}
data Enigma = SimpleEnigma Rotor Rotor Rotor Reflector|
               SteckeredEnigma Rotor Rotor Rotor Reflector Stecker

defaultEnigma :: Enigma -- simple enigma with predifined values
defaultEnigma = SimpleEnigma rotor1 rotor2 rotor3 defaultReflector

defaultEnigma2 :: Enigma --stekered enigma with predifined values
defaultEnigma2 = SteckeredEnigma rotor1 rotor2 rotor3 defaultReflector defaultSteckerboard

offset_step :: Offsets->Offsets
offset_step (l,m,25) = offsetlm (l,m,0)
offset_step (l,m,r) = (l,m,r+1)
 
offsetlm :: Offsets->Offsets
offsetlm(l,25,0)=offsetl (l,0,0)
offsetlm (l,m,0)=(l,m+1,0)
 
offsetl :: Offsets->Offsets
offsetl (25,0,0)= (0,0,0)
offsetl (l,0,0)=(l+1,0,0)
 
 -- n steps
offsetN :: Offsets->Int->Offsets
offsetN os 0 = os
offsetN os n = offsetN (offset_step os) (n-1)


defaultReflector ::  Reflector -- Reflector with predifend values
defaultReflector =  [('A', 'Y'),
					 ('B', 'R'),
					 ('C', 'U'),
					 ('D', 'H'),
					 ('E', 'Q'),
					 ('F', 'S'),
					 ('G', 'L'),
					 ('I', 'P'),
					 ('J', 'X'),
					 ('K', 'N'),
					 ('M', 'O'),
					 ('T', 'Z'),
					 ('V', 'W')]
			 
defaultSteckerboard :: Stecker -- Stekerboard with predifined values
defaultSteckerboard = [('A', 'Y'),
						 ('B', 'R'),
						 ('C', 'U'),
						 ('D', 'H'),
						 ('E', 'Q'),
						 ('F', 'S'),
						 ('G', 'L'),
						 ('I', 'P'),
						 ('J', 'X'),
						 ('K', 'N')]

						 
{- function return letter position in a set of characters-}
getIndex :: Char -> [Char] -> Int -> Int
getIndex ch array i
	|i >= length array = -1 -- return -1 if the letter was not found
	|array!!i == ch = i --return the index
	|otherwise = getIndex ch array (1+i) -- continue searching the list

{- Function swaps the characters if the character was found in a set of characters-}	
reflect :: Char -> [(Char,Char)] ->  Char
reflect c reflector
	|0== length reflector =c -- if letter was not found leave it as it was
	|c==(fst (head reflector)) = snd(head reflector) -- if letter matches swap letters
	|c==(snd (head reflector)) = fst(head reflector) -- if letter matches swap letters
	|otherwise = reflect c (tail reflector) -- otherwise continue checking the list	

{-Function encodes letter with current rotor-}
encode :: Char -> Rotor -> Int -> Char
encode  cha rotor offset
	|getIndex cha ['A'..'Z'] 0 == -1 = cha -- if the letter was not found return the letter without encoding
	|offset >25 = encode cha rotor (offset - 26) -- if offset is too large decrease it
	|((getIndex cha ['A'..'Z'] 0) - offset) < 0 = rotor!!((getIndex cha ['A'..'Z'] 0) - offset + 26) --if after applying offset the number is below zero, increase the number to get required character
	|otherwise = rotor!!((getIndex cha ['A'..'Z'] 0) - offset)--just return required character

{- function checks for a position in rotor character list and returns a letter from plain alphebet list-}	
decode :: Char -> Rotor -> Int -> Char
decode  cha rotor offset
	|getIndex cha rotor 0 == -1 = cha -- if the letter was not found return the letter without encoding
	|offset >25 = encode cha ['A'..'Z'] (offset - 26) --if offset is too large decrease it
	|((getIndex cha rotor 0) + offset) > 25 = ['A'..'Z']!!((getIndex cha rotor 0) + offset - 26)  --if after applying offset the number is below zero, increase the number to get required character
	|otherwise = ['A'..'Z']!!((getIndex cha rotor 0) + offset) --just return required character

{-Function which fully encodes the given letter using all 3 rotors and reflector-}	
enigmaEncode :: Char ->Enigma->Offsets -> Char
enigmaEncode ch (SimpleEnigma lr mr rr ref) (ol,om,or) =  decode --function ror encoding without stakerboard
														   (decode
															   (decode 
																  (reflect 
																	(encode 
																	   (encode 
																		  (encode ch rr or) 
																		  mr om) 
																	   lr ol) 
																	ref) 
																  lr ol) 
															   mr om)
															rr or
enigmaEncode ch (SteckeredEnigma lr mr rr ref sb) (ol,om,or) = (reflect (decode --function ror encoding without stakerboard
																					   (decode
																						   (decode 
																							  (reflect 
																								(encode 
																								   (encode 
																									  (encode (reflect ch sb) rr or) 
																									  mr om) 
																								   lr ol) 
																								ref) 
																							  lr ol) 
																						   mr om)
																						rr or) 
																					sb)

{-function calls enigmaEncode function for every letter in string-}
enigmaEncodeMessage:: String ->Enigma->Offsets -> String
enigmaEncodeMessage message (SimpleEnigma lr mr rr ref) (ol,om,or)--function ror encoding without stakerboard
			| null message = ""
			|otherwise = (enigmaEncode (head message) (SimpleEnigma lr mr rr ref) (ol,om,or)):(enigmaEncodeMessage (tail message) (SimpleEnigma lr mr rr ref) (ol,om,or+1))
enigmaEncodeMessage message (SteckeredEnigma lr mr rr ref sb) (ol,om,or) --function ror encoding with stakerboard
			| null message = ""
			|otherwise = (enigmaEncode (head message) (SteckeredEnigma lr mr rr ref sb) (ol,om,or)):(enigmaEncodeMessage (tail message) (SteckeredEnigma lr mr rr ref sb) (ol,om,or+1)) 
	

