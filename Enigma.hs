
{- 
    COM2001 - Advanced Programming Topics
    Assignment 2 - The Enigma Machine

    Vaclav Hudec

    date: 06/11/2010
-}

module Enigma where

    -- encoding and reverse encoding functions
    import Ciphers
    
    -- data structures
    type Rotor = String
    type Reflector = Cipher
    type SimpleEnigma = ([Rotor], Reflector)
    type Steckerboard = Cipher
    type Offsets = [Int]
    
    -- since rotors and their offsets are passed in lists,
    -- there can be any number of rotors used in Enigma
    -- (the number of rotors must be equal to the number of offsets)
    data Enigma = SimpleEnigma SimpleEnigma | SteckerEnigma SimpleEnigma Steckerboard
    
    -- rotors
    rotor1 :: Rotor
    rotor2 :: Rotor
    rotor3 :: Rotor
    rotor4 :: Rotor
    rotor5 :: Rotor
    
    rotor1 = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"
    rotor2 = "AJDKSIRUXBLHWTMCQGZNPYFVOE"
    rotor3 = "BDFHJLCPRTXVZNYEIWGAKMUSQO"
    rotor4 = "ESOVPZJAYQUIRHXLNFTGKDCMWB"
    rotor5 = "VZBRGITYUPSDNHLXAWMJQOFECK"
    
    -- plain
    plain :: String
    plain = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    
    -- reflector
    reflector :: Reflector
    reflector = [   ('A', 'Y'),
                    ('B', 'R'),
                    ('C', 'U'),
                    ('D', 'H'),
                    ('E', 'Q'),
                    ('F', 'S'),
                    ('G', 'L'),
                    ('I', 'P'),
                    ('J', 'X'),
                    ('K', 'N'),
                    ('M', 'O'),
                    ('T', 'Z'),
                    ('V', 'W')]
    
    -- stecker boad            
    myboard :: Steckerboard
    myboard = [ ('X', 'Y'),
                ('H', 'F'),
                ('T', 'E'),
                ('M', 'Q'),
                ('J', 'R'),
                ('P', 'N'),
                ('W', 'V'),
                ('L', 'D'),
                ('A', 'Z'),
                ('S', 'U')]
    
    {-**************************************************************************                
    *** enigma encoding functions **********************************************
    **************************************************************************-}
    
    enigmaEncode :: Char->Enigma->[Int] -> Char -- call auxiliary encode function with additional information
    enigmaEncode char enigma [a,b,c] = enigmaEncodeA char enigma [c,b,a] 0 False
    
    -- auxiliary encode function takes, in addition, the rotors' index
    -- and a boolean information whether the encoding process has reached the reflector
    enigmaEncodeA :: Char->Enigma->[Int]->Int->Bool -> Char
     
    -- main encoding algorithm for simple Enigma
    enigmaEncodeA char (SimpleEnigma ([r1,r2,r3], ref)) offsets i reflected
        | i == numRotors && reflected = char  -- stopping condition, the encoding process was reflected and went through all rotors again
        | i < numRotors = enigmaEncodeA -- encode given character by each rotor until you reach the reflector
                    (method char (zip plain (rotors!!i)) (offsets!!i))  -- method (encode or reverseEncode),
                                                                        -- given char, first cipher and first rotor's offset
                    simpleEnigma    -- set up of simple Enigma
                    offsets -- offsets for all rotors remain unchanged
                    (i + 1) -- index of next rotor to go through
                    reflected   -- boolean -> has the encoding process reached the reflector and been sent back thru rotors?
        | otherwise = enigmaEncodeA -- the encoding process has reached the reflector
                        (reflect char ref)  -- reflect the character
                        (SimpleEnigma ((reverse rotors), ref))   -- set up of simple Enigma, rotors reversed as the
                                                                        -- encoding process now enters the rotors conversely
                        (reverse offsets)   -- as well as rotors, we also have to reverse their offsets
                        0   -- rotors' index set to go through the first rotor
                        True    -- the reflector has been reached -> true
            where   method = if reflected then reverseEncode else encode    -- depends on whether the encoding process
                                                                            -- is going towards reflector (encode) or
                                                                            -- the other way around (reverseEncode) 
                    simpleEnigma = SimpleEnigma (rotors, ref)
                    numRotors = length rotors
                    rotors = [r3,r2,r1]
    
    -- main encoding algorithm for steckered Enigma
    enigmaEncodeA char (SteckerEnigma simpleEnigma board) offsets i _ =
            -- 'stecker' the character, encode using simple Enigma, and 'stecker' returned character again
        reflect (enigmaEncodeA (reflect char board) (SimpleEnigma simpleEnigma) offsets i False) board

    -- reflect (swap) the character at the reflector
    reflect :: Char->Reflector -> Char
    reflect char [] = char -- if the pair-character is not found, return the input one -> this can also be used for
    reflect char (head:tail)                                                           -- 'steckering' in Steckered Enigma
        | char == fst head = snd head
        | char == snd head = fst head
        | otherwise = reflect char tail -- pair-character not found in the head, continue searching the tail

    enigmaEncodeMessage :: String->Enigma->[Int] -> String -- can take both Simple and Steckered Enigmas
    enigmaEncodeMessage [] _ _ = [] -- we reached the end of the message being encoded
    
    -- encode the head of the message and continue the process recursively on the tail
    enigmaEncodeMessage (first:rest) enigma offsets =
        (enigmaEncode first enigma (reverse offsets)):(enigmaEncodeMessage rest enigma advancedOffsets)
            where advancedOffsets = advanceOffsets offsets -- update the offsets for each rotor
        
    -- advances the rotors' offsets accordingly
    -- if any of the offsets is set to more than 25 by the user, it goes to 0 once "a key has been pressed on Enigma"    
    advanceOffsets :: [Int] -> [Int]
    advanceOffsets [o1, o2, o3]
        | advanced > 25 = advanceOffsets [-1, o2 + 1, o3]
        | o2 > 25 = advanceOffsets [o1, 0, o3 + 1]
        | o3 > 25 = [advanced, o2, 0]
        | otherwise = [advanced, o2, o3]
            where advanced = o1 + 1;

    
    advanceN :: Offsets->Int-> Offsets
    advanceN [o1,o2,o3] i = offsetN [o3,o2,o1] i
    
    offsetN :: Offsets->Int-> Offsets
    offsetN [o1,o2,o3] 0 = [o3,o2,o1]
    offsetN o i = offsetN (advanceOffsets o) (i-1)
        




    
    
    