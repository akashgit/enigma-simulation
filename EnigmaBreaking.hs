-- COM2001 - Assignment 3
-- Vaclav Hudec (aca09vh)
-- 15/12/2010

module EnigmaBreaking where

import Enigma
import Char

type Connection = (Int, Char, Char)
type SteckerPair = (Char, Char)
type Stecker = [SteckerPair]
type Crib = Stecker

result :: Maybe a -> a
result (Just a) = a

mycribTest :: Crib
mycribTest = zip "WETTERVORHERSAGEBISKAYA" "RWIVTYRESXBFOGKUHQBAISE"

mycrib1 :: Crib
mycrib1 = zip "AIDEGHMC" "IDEGHMCL"

mycrib2 :: Crib
mycrib2 = zip "UNIVERSITYOFSHEFFIELDSTOPDEPARTMENTOFCOMPUTERSCIENCESTOP" "TWPABGYBZHTWFQQVZLZKWEMPNIYNYQEYYJFRKBEOQVMSTPGZQGDVEVGA"

mycrib3 :: Crib
mycrib3 = zip "UNIVERSITYOFSHEFFIELDDEPARTMENTOFCOMPUTERSCIENCESTOP" "XVJWKITFINXQQPVIRHVCCZKWWEHFNYSYUPZLLTSWQEUEFOZVKCUT"

breakEnigma :: Crib->Int-> Maybe (Offsets, Stecker)
breakEnigma crib start = breakEA crib stecker [0,0,start]
    where   stecker = [(letter,'A')] --initial stecker
            letter = fst (crib!!start) --letter in the plain at initial position

breakEA :: Crib->Stecker->Offsets-> Maybe (Offsets, Stecker)
breakEA crib stecker [o1,o2,o3]
    | offsets == [25,25,25] && fs == Nothing = Nothing --all offsets tried, find stecker empty -> we return nothing
    | fs == Nothing = breakEA crib stecker (advanceN offsets 1) --try next offset
    | otherwise = Just ([o3+1,o2,o1], result fs) --return found stecker and offsets
    where   fs = findStecker crib stecker offsets
            offsets = [o1,o2,o3]

letters :: String
letters = "ZYXWVUTSRQPONMLKJIHGFEDCBA"

findStecker :: Crib->Stecker->Offsets-> Maybe Stecker
findStecker crib stecker offsets = findSteckerA crib stecker offsets 24 --we have 24 other letters to try

findSteckerA :: Crib->Stecker->Offsets->Int-> Maybe Stecker
findSteckerA crib ((a,b):t) offsets i
	| ci /= Nothing = ci --return found stecker
	| nextAssumption i /= Nothing = findSteckerA crib ((a, result(nextAssumption i)):t) offsets (i-1) --try another assumption
	| otherwise = Nothing --report failure
	where ci = chaseImplications ((a,b):t) crib offsets 
	
-- get a another letter	
nextAssumption :: Int-> Maybe Char
nextAssumption i
    | i < 0 = Nothing --we have gone thru all letters
    | otherwise = Just (letters!!i) -- return next

chaseImplications :: Stecker->Crib-> Offsets->Maybe Stecker
chaseImplications [] _ _ = Nothing --all implications tried out
chaseImplications (head:tail) crib offsets = followConnections fc crib (head:tail) offsets
    where fc = findConnections head crib --find connection for the first stecker pair
                
findConnections :: SteckerPair->Crib-> [Connection]
findConnections (a,b) crib = [(p,myreflect i [(a,b)],o)|(p,i,o)<-cribTemp,i == a || i == b] --return list of found triples (connections)
    where cribTemp = zip3 ([0..(length crib) - 1]) (map fst crib) (map snd crib) --create triple, adding position to crib letter pairs

followConnections :: [Connection]->Crib->Stecker->Offsets-> Maybe Stecker
followConnections [] _ stecker _ = Just stecker
followConnections (head:tail) crib stecker offsets
    | (fc /= Nothing) = followConnections tail crib (result fc) offsets --go to another connection
    | otherwise = Nothing --contradiction found, report failure
    where fc = followConnection head crib stecker offsets --follow another connection

followConnection :: Connection->Crib->Stecker->Offsets-> Maybe Stecker  
followConnection (n,i,c) crib stecker offsets
    | sa == Nothing = Nothing --contradicition found
    | sa == Just stecker = Just stecker --compatible, but present
    | otherwise = chaseImplications (result sa) crib offsets --added a new pair to stecker and chase implications of this pair
    where out = enigmaEncode i (SimpleEnigma ([rotor1, rotor2, rotor3], reflector)) (advanceN offsets (n+1)) --encode letter
          sa = steckerAdd (out,c) stecker

steckerAdd :: SteckerPair->Stecker -> Maybe Stecker
steckerAdd pair stecker
    | not (duplicated pair stecker) = Just (pair:stecker) --compatible and not yet present -> add new pair to the stecker
    | compatible pair stecker = Just stecker --compatible, but present -> do not add anything and return original stecker
    | otherwise = Nothing --contradiction found, report failure
                
-- is a pair or one of ins letters already present?
duplicated :: SteckerPair->Stecker-> Bool
duplicated _ [] = False --no duplication found in the list
duplicated (a,b) (head:tail)
    | (fst head /= a && snd head /= a) &&
      (fst head /= b && snd head /= b) = duplicated (a,b) tail
    | otherwise = True --duplication found
    
-- is a new pair compatible?
compatible :: SteckerPair->Stecker-> Bool
compatible _ [] = True --compatible
compatible (a,b) stecker
    | ((fst head == a && snd head == b) ||
      (fst head == b && snd head == a)) || not (duplicated (a,b) [head]) = compatible (a,b) tail -- compatible if both letters
    | otherwise = False --not compatible                                                         -- in the pair are the same
    where (head:tail) = stecker                                                                  -- or both different   
    
-- reflect letters in a pair
myreflect :: Char->Reflector -> Char
myreflect char (head:tail)
    | char == fst head = snd head
    | char == snd head = fst head